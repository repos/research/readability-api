import os
import re
import requests
from flask import Flask, request, jsonify, render_template
from flask_cors import CORS
from mwconstants import WIKIPEDIA_LANGUAGES
import yaml
import pickle
import utils

app = Flask(__name__)

__dir__ = os.path.dirname(__file__)
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, 'default_config.yaml'))))
try:
    app.config.update(
        yaml.safe_load(open(os.path.join(__dir__, 'config.yaml'))))
except IOError:
    # It is ok if there is no local config file
    pass

# Enable CORS for API endpoints
#CORS(app, resources={'*': {'origins': '*'}})
CORS(app)

# app = Flask(__name__)
# app.config["DEBUG"] = True
# app.config['JSON_SORT_KEYS'] = False
# CUSTOM_UA = 'readability-api -- mgerlach@wikimedia.org'

print("Try: http://127.0.0.1:5000/api/v1?lang=en&title=Monty_Buell")

## locally: flask run

@app.route('/')
def index():
    return render_template('index.html', page_title=set_title(), lang=set_lang())
    # return 'Server Works!'

@app.route('/api/v1', methods=['GET'])
def get_readability_score():
    wikilang = set_lang()
    page_title = set_title()

    # get revid
    revid = utils.get_current_revid(page_title, wikilang)
    dict_results = utils.get_readability_score_lw(revid,wikilang)
    dict_results_formatted = {
        "readability_score": round(dict_results["output"]["score"],2),
        "predicted_grade_level": round(dict_results["output"]["fk_score_proxy"],2)
    }

    dict_out = {
        "lang": wikilang,
        "page_title": page_title,
        "article": "https://{0}.wikipedia.org/wiki/{1}".format(wikilang,page_title),
        "results": dict_results_formatted
    }
    return jsonify(dict_out)

def set_lang():
    if 'lang' in request.args:
        lang = request.args['lang'].lower()
        if lang in WIKIPEDIA_LANGUAGES:
            return lang
    return None

def set_title():
    if 'page_title' in request.args:
        title = request.args['page_title']
    elif 'title' in request.args:
        title = request.args['title']
    else:
        title = None

    if title:
        title = title.replace('_', ' ').strip()
        try:
            title = title[0].capitalize() + title[1:]
        except IndexError:
            title = None
    return title

if __name__ == '__main__':
    '''
    '''
    app.run(host='0.0.0.0')
