import re
import requests
import json

def resolve_redirect_title(page_title, lang):
    api_url_base = 'https://%s.wikipedia.org/w/api.php'%(lang)
    headers = {"User-Agent": "MGerlach_(WMF) | WMF-Research"}
    params = {
        'action': 'query',
        'titles': page_title,
        'redirects': "True",
        'formatversion': "2",
        'format': 'json',
    }
    response = requests.get( api_url_base,params=params, headers=headers).json()
    page_title_resolved = response["query"]["pages"][0]["title"]
    return page_title_resolved

def get_current_revid(page_title, lang):
    # resolved the redirect to get correct revid
    page_title_resolved = resolve_redirect_title(page_title, lang)
    api_url_base = 'https://%s.wikipedia.org/w/api.php'%(lang)
    headers = {"User-Agent": "MGerlach_(WMF) | WMF-Research"}
    params = {
        'action': 'query',
        'titles': page_title_resolved,
        'prop': 'revisions',
        'rvprop': 'ids',
        'rvlimit': 1,
        'format': 'json',
    }
    response = requests.get( api_url_base,params=params, headers=headers).json()
    revid = list(response["query"]["pages"].values())[0]["revisions"][0]["revid"]
    return revid

def get_readability_score_lw(revid, lang):
    "takes the revid and lang to get readability score"
    inference_url = 'https://api.wikimedia.org/service/lw/inference/v1/models/readability:predict'
    headers = {
        "User-Agent": "MGerlach_(WMF) | WMF-Research",
        'Content-type': 'application/json'
    }
    data_in = {"rev_id": revid, "lang": lang}
    response = requests.post(inference_url, headers=headers, data=json.dumps(data_in))
    result = response.json()
    return result